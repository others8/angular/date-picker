import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit, OnChanges {

  date = new FormControl({ disabled: false, value: '' });

  @Input() disabled = false;

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges() {
    this.date.reset({disabled: this.disabled, value: ''} );
  }

}
