import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'date-picker-ngx';

  checked = false;

  constructor() {

  }

  ngOnInit() {

  }

  onToggle(param) {
    this.checked = param.checked;
  }

  onAuthorClick() {
    // alert('working');
    window.open('https://www.linkedin.com/in/udit-mishra/');
  }

}
